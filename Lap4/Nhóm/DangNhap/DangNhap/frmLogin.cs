﻿using System.Data;
using System.Data.SqlClient;

namespace DangNhap
{
    public partial class frmLogin : Form
    {
        string strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        Encryption encryption;
        SqlCommand cmd;
        public frmLogin()
        {            
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }

            var TaiKhoan = txtLogin.Text.Trim();  
            string MatKhau = txtpassword.Text.Trim();
            byte[] MHMatKhau = CreateMD5(MatKhau);
            MatKhau = ConvertByteToHexa(MHMatKhau);
            cmd = new SqlCommand("SP_CHECK_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;

            SqlDataReader kq = cmd.ExecuteReader();

            if (kq.Read())
            {
                if (kq[0].ToString() == MatKhau)
                {
                    MessageBox.Show("Chào bạn nhân viên" + kq[1].ToString());
                }
                else
                {
                    cmd = new SqlCommand("SP_CHECK_SINHVIEN", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;
                    kq = cmd.ExecuteReader();
                    if (kq.Read())
                    {
                        if (kq[0].ToString() == MatKhau)
                        {
                            MessageBox.Show("Chào bạn sinh viên" + kq[1].ToString());
                        }
                        else
                        {
                        }
                    }
                }
            }
            


                SqlCommand cmd_SV = new SqlCommand("SP_CHECK_SV", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd_SV.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;
            cmd_SV.Parameters.Add("@MATKHAU", SqlDbType.VarChar).Value = MatKhau;
            
            using SqlDataReader kq_SV = cmd_SV.ExecuteReader();
            
            if (kq_SV.Read())
            {
                MessageBox.Show("Đăng Nhập thành công " + kq_SV[0]);
            }
            else
            {
                sqlconn.Close();

                if (sqlconn.State == ConnectionState.Closed)
                {
                    sqlconn.Open();
                }
                using SqlDataReader kq_NV = cmd_NV.ExecuteReader();
                if (kq_NV.Read())
                {
                    string MANV = kq_NV[1].ToString();
                    frmManagerClass frmManagerStudent = new frmManagerClass(MANV,MatKhau);
                    MessageBox.Show("Đăng Nhập thành công " + kq_NV[0]);
                    this.Hide();
                    frmManagerStudent.ShowDialog();
                    frmManagerStudent = null;  
                    this.Show();
                    txtLogin.Text = "";
                    txtpassword.Text = "";

                }
                else
                {
                    MessageBox.Show("Tên đăng nhập và mật khẩu không hợp lệ");
                }
                sqlconn.Close();
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public static byte[] CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                return hashBytes;

            }
        }
        public static string ConvertByteToHexa(byte[] hashBytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}