﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace DangNhap
{
    public partial class frmDiem : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        private String MALOP,MATKHAU;
        public frmDiem(String Malop, String MatKhau)
        {
            MALOP = Malop; MATKHAU = MatKhau;
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }

        private void frmDiem_Load(object sender, EventArgs e)
        {
            frm_Load();
        }
        private void frm_Load()
        {
            dgvDiem.DataSource = fetchDiem();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
            textBox4.ReadOnly = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            
            String HoTen = textBox2.Text;
            String Diem = textBox3.Text;
            String MaSV = textBox4.Text;
            cmd = new SqlCommand("SP_UPD_DIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MASV", SqlDbType.VarChar, 50).Value = MaSV;
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = HoTen;
            cmd.Parameters.Add("DIEM", SqlDbType.Float).Value = Diem;
            cmd.ExecuteReader();
            frm_Load();
        }

        private void dgvDiem_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow dataGridViewRow = dgvDiem.Rows[e.RowIndex];
            String MaSV = dataGridViewRow.Cells[0].Value.ToString();
            String HoTen = dataGridViewRow.Cells[1].Value.ToString();
            String Diem = dataGridViewRow.Cells[2].Value.ToString();

            textBox4.Text = MaSV;
            textBox2.Text = HoTen;
            textBox3.Text = Diem;


            textBox2.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox3.ReadOnly = true;
        }

        private DataTable fetchDiem()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_DIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 20).Value = MALOP;
            cmd.Parameters.Add("MATKHAU", SqlDbType.NVarChar, 20).Value = MATKHAU;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;

        }
    }
}
