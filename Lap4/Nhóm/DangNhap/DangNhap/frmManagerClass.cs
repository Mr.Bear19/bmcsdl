﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace DangNhap
{
    public partial class frmManagerClass : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        private String MANV ,MATKHAU;
        public frmManagerClass(String MANV,String MatKhau)
        {
            this.MANV = MANV;
            this.MATKHAU = MatKhau;
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }

        private void dGVSinhVien_load(object sender, EventArgs e)
        {
            //dGVSinhVien.AutoGenerateColumns = false;
            dGVLop_data();
            cBxNhanVien.DisplayMember = "HOTEN";
            cBxNhanVien.DataSource = FetchNameNV();
        }
        private void dGVLop_data()
        {
            dGVLop.DataSource = FetchLop();
        }
        private DataTable FetchLop()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter  sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            
            return dt;
        }

        private DataTable FetchNameNV()
        {
            DataTable dt = new DataTable();
            if(sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_NV", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dGVSinhVien_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1) return;
            DataGridViewRow dataGridViewRow = dGVLop.Rows[e.RowIndex];
            string MALOP = dataGridViewRow.Cells[0].Value.ToString();
            frmManagerStudent frmManagerStudent = new frmManagerStudent(MANV,MALOP,MATKHAU);
            this.Hide();
            frmManagerStudent.ShowDialog();
            this.Show();
        }

        private void frmManagerStudent_Click(object sender, EventArgs e)
        {
            txtMaLop.Text = "";
            txtMaLop.ReadOnly = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CHECK_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (!kq.HasRows)
            {
                MessageBox.Show("Mã lớp không tồn tại!!");
            }
            else
            {
                try
                {
                    var tenlop = txtTenLop.Text.Trim();
                    var tennv = cBxNhanVien.Text.Trim();
                    if (sqlconn.State == ConnectionState.Closed)
                    {
                        sqlconn.Open();
                    }
                    cmd = new SqlCommand("SP_UPD_LOP", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
                    cmd.Parameters.Add("@TENLOP", SqlDbType.NVarChar, 100).Value = tenlop;
                    cmd.Parameters.Add("@HOTEN", SqlDbType.NVarChar, 50).Value = tennv;
                    cmd.ExecuteReader();
                    dGVLop_data();
                }
                catch
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!!!");
                }
            }
                

        }

        private void btnTailai_Click(object sender, EventArgs e)
        {
            dGVLop_data();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CHECK_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP",SqlDbType.VarChar,20).Value= malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if(kq.HasRows)
            {
                MessageBox.Show("Mã lớp đã tồn tại!!");
            }
            else
            {
                try
                {
                    var tenlop = txtTenLop.Text.Trim();
                    var tennv = cBxNhanVien.Text.Trim();
                    if (sqlconn.State == ConnectionState.Closed)
                    {
                        sqlconn.Open();
                    }
                    cmd = new SqlCommand("SP_INS_LOP", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
                    cmd.Parameters.Add("@TENLOP", SqlDbType.NVarChar, 50).Value = tenlop;
                    cmd.Parameters.Add("@HOTEN", SqlDbType.NVarChar, 50).Value = tennv;
                    cmd.ExecuteNonQuery();
                    dGVLop_data();
                }
                catch
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!!!");
                }

            }
           
        }

        private void dGVSinhVien_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1) return;
            DataGridViewRow dataGridViewRow = dGVLop.Rows[e.RowIndex];
            txtMaLop.Text = dataGridViewRow.Cells[0].Value.ToString();
            txtTenLop.Text = dataGridViewRow.Cells[1].Value.ToString();
            txtMaLop.ReadOnly = true;
            cBxNhanVien.Text = dataGridViewRow.Cells[2].Value.ToString();
        }
    }

 }

