﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Collections;

namespace WinFormsApp
{
    public partial class frmDiem : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
        SqlConnection sqlconn = null;
        private SqlCommand cmd;
        private String MALOP,MANV;
        private static ArrayList ListMASV = new ArrayList();
        private static ArrayList ListHOTEN = new ArrayList();
        private static ArrayList ListDIEM = new ArrayList();
        private byte[] plaintext;
        
        Encoding ByteConverter = new UnicodeEncoding();
        public frmDiem(String Malop, String MaNV)
        {
            MALOP = Malop; MANV = MaNV;
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
        }

        private void frmDiem_Load(object sender, EventArgs e)
        {

            frm_Load();
        }
        private void frm_Load()
        {         
            fetchDiem();

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            txtSV.ReadOnly = false;
            txtDiem.ReadOnly = false;
            txtMaSV.ReadOnly = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            
            String HoTen = txtSV.Text;
            String Diem = txtDiem.Text;
            String MaSV = txtMaSV.Text;
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            cmd = new SqlCommand("SP_SEL_PUB_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MANV", SqlDbType.VarChar, 20).Value = MANV;
            SqlDataReader kq = cmd.ExecuteReader();
            if (kq.Read())
            {
                string PUB = kq[0].ToString();
                rsa.FromXmlString(PUB);
            }
            plaintext = ByteConverter.GetBytes(Diem);
            byte[] encryptedtext = RSAEncrypt(plaintext, rsa.ExportParameters(false), false);
            cmd = new SqlCommand("SP_UPD_DIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MASV", SqlDbType.VarChar, 50).Value = MaSV;
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = MALOP;
            cmd.Parameters.Add("DIEM", SqlDbType.VarBinary).Value = encryptedtext;
            cmd.ExecuteReader();
            frm_Load();
        }

        private void dgvDiem_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridViewRow dataGridViewRow = dgvDiem.Rows[e.RowIndex];
                String MaSV = dataGridViewRow.Cells[0].Value.ToString();
                String HoTen = dataGridViewRow.Cells[1].Value.ToString();
                String Diem = dataGridViewRow.Cells[2].Value.ToString();

                txtMaSV.Text = MaSV;
                txtSV.Text = HoTen;
                txtDiem.Text = Diem;


                txtSV.ReadOnly = true;
                txtMaSV.ReadOnly = true;
                txtDiem.ReadOnly = true;
            }
            catch
            {

            }
        }
        
        private void fetchDiem()
        {

            RSACryptoServiceProvider rsa1 = new RSACryptoServiceProvider();
            cmd = new SqlCommand("SP_SEL_PRI_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MANV", SqlDbType.VarChar, 20).Value = MANV;
            SqlDataReader kq = cmd.ExecuteReader();
            if (kq.Read())
            {
                string PRI = kq[0].ToString();
                rsa1.FromXmlString(PRI);
            }
            cmd = new SqlCommand("SP_SEL_DIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 20).Value = MALOP;
            SqlDataReader ds = cmd.ExecuteReader();
            while (ds.Read())
            {                
                ListMASV.Add(ds["MASV"].ToString());
                ListHOTEN.Add(ds["HOTEN"].ToString());
                byte[] DIEM = (byte[])ds["DIEMTHI"];
                byte[] dencryptedtext = RSADecrypt(DIEM, rsa1.ExportParameters(true));
                string DiemThi = ConvertByteToHexa(dencryptedtext);
                if(DiemThi.Length <5)
                    ListDIEM.Add(DiemThi[1]);
                else
                    ListDIEM.Add("10");
            }
            dgvDiem.Rows.Clear();
            for (int i = 0; i < ListMASV.Count; i++)
            {
                DataGridViewRow newRow = new DataGridViewRow();

                newRow.CreateCells(dgvDiem);
                newRow.Cells[0].Value = ListMASV[i];
                newRow.Cells[1].Value = ListHOTEN[i];
                newRow.Cells[2].Value = ListDIEM[i];
                dgvDiem.Rows.Add(newRow);
            }
            ListMASV.Clear(); ListHOTEN.Clear(); ListDIEM.Clear();

        }
        

        private void btnThem_Click(object sender, EventArgs e)
        {
            string hoten = txtSV.Text.Trim();
            string masv = txtMaSV.Text.Trim();
            string DIEM = txtDiem.Text.ToString();
            cmd = new SqlCommand("SP_CHECK_INS_DIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MASV", SqlDbType.VarChar, 20).Value = masv;
            cmd.Parameters.Add("MALOP",SqlDbType.VarChar, 20).Value = MALOP;
            SqlDataReader sqlDataReader1 = cmd.ExecuteReader();
            if (sqlDataReader1.Read())
            {
                cmd = new SqlCommand("SP_CHECK_DIEM", sqlconn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add("MASV",SqlDbType.VarChar,20).Value = masv;

                SqlDataReader sqlDataReader2 = cmd .ExecuteReader();
                if (sqlDataReader2.Read())
                {
                    MessageBox.Show("Sinh viên đã có điểm!");
                }
                else
                {
                    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                    cmd = new SqlCommand("SP_SEL_PUB_NHANVIEN", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("MANV", SqlDbType.VarChar, 20).Value = MANV;
                    SqlDataReader kq = cmd.ExecuteReader();
                    if (kq.Read())
                    {
                        string PUB = kq[0].ToString();
                        rsa.FromXmlString(PUB);
                    }
                    plaintext = ByteConverter.GetBytes(DIEM);
                    byte[] encryptedtext =  RSAEncrypt(plaintext, rsa.ExportParameters(false), false);
                    cmd = new SqlCommand("SP_INS_DIEM", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("MASV", SqlDbType.VarChar, 20).Value = masv;
                    cmd.Parameters.Add("MALOP", SqlDbType.NVarChar, 100).Value = MALOP;
                    cmd.Parameters.Add("DIEM", SqlDbType.VarBinary).Value = encryptedtext;
                    cmd.ExecuteReader();
                    frm_Load();
                    
                }
            }
            else
            {
                MessageBox.Show("Mã số sinh viên kh hợp lệ!");
            }
        }
        public byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo)
        {
            byte[] decryptedData;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);
                decryptedData = RSA.Decrypt(DataToDecrypt, false);
            }
            return decryptedData;
        }
            
        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            byte[] encryptedData;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);

                encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
            }
            return encryptedData;
        }

        public static string ConvertByteToHexa(byte[] hashBytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
