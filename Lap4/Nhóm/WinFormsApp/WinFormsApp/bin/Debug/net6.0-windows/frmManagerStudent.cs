﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace WinFormsApp
{
    public partial class frmManagerStudent : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        private String MANV,MALOP,MATKHAU;
        public frmManagerStudent(String Manv,String Malop,String MatKhau)
        {
            this.MANV = Manv;
            this.MALOP = Malop;
            this.MATKHAU = MatKhau; 
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }

        private void frmManagerStudent_Load(object sender, EventArgs e)
        {
            frm_load();
        }
        private void frm_load()
        {
            dgvSinhVien_Data();
            cbxNhanVien.DisplayMember = "HOTEN";
            cbxNhanVien.DataSource = fetchNameNV();
            cbxLopHoc.DisplayMember = "TENLOP";
            cbxLopHoc.DataSource = fetchNameLOP();
            txtSoLuong.Text = fetchSoLuongSinhVien(MALOP).ToString();
            txtSoLuong.ReadOnly = true;
            txtMaSV.ReadOnly = true;
            cbxNhanVien.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        private void dgvSinhVien_Data()
        {
            dgvSinhVien.DataSource = fetchSinhVien();
        }

        private void dgvSinhVien_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow dataGridViewRow = dgvSinhVien.Rows[e.RowIndex];
            DateTime  Datetime = Convert.ToDateTime(dataGridViewRow.Cells[2].Value.ToString());
            String MaSV = dataGridViewRow.Cells[0].Value.ToString();
            String HoTen = dataGridViewRow.Cells[1].Value.ToString();
            String DiaChi = dataGridViewRow.Cells[3].Value.ToString();
            String TenDN = dataGridViewRow.Cells[4].Value.ToString();

            txtMaSV.Text = MaSV;
            txtHoTen.Text = HoTen;
            dateTimePicker1.Value = Datetime;
            txtDiaChi.Text = DiaChi;
            txtTenDN.Text = TenDN;

            
            txtHoTen.ReadOnly = true;
            txtDiaChi.ReadOnly = true;
            txtTenDN.ReadOnly = true;
            txtMatKhau.ReadOnly = true;
        }

        private DataTable fetchSinhVien()
        {
            DataTable dt = new DataTable();
            if(sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_SINHVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 20).Value = MALOP;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private DataTable fetchNameNV()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private DataTable fetchNameLOP()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }

        private void btnTailai_Click(object sender, EventArgs e)
        {
            frm_load();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            txtHoTen.ReadOnly = false;
            txtDiaChi.ReadOnly = false;
            txtTenDN.ReadOnly = false;
            txtMatKhau.ReadOnly = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            String MaSV = txtMaSV.Text;
            String HoTen = txtHoTen.Text;
            String DiaChi = txtDiaChi.Text;
            String TenDN = txtTenDN.Text;
            String MatKhau = txtMatKhau.Text;
            DateTime NgaySinh = dateTimePicker1.Value;
            
            if(MatKhau == "")
            {
                cmd = new SqlCommand("SP_UPD_SV_NO_PASSWORD", sqlconn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add("MASV",SqlDbType.VarChar,50).Value = MaSV;
                cmd.Parameters.Add("HOTEN", SqlDbType.NVarChar, 50).Value = HoTen;
                cmd.Parameters.Add("NGAYSINH", SqlDbType.DateTime).Value = NgaySinh;
                cmd.Parameters.Add("DIACHI", SqlDbType.NVarChar, 100).Value = DiaChi;
                cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = MALOP;
                cmd.Parameters.Add("TENDN", SqlDbType.VarChar, 50).Value = TenDN;
                cmd.ExecuteReader();
                frm_load();
            }
            else
            {
                cmd = new SqlCommand("SP_UPD_SV_PASSWORD", sqlconn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add("MASV", SqlDbType.VarChar, 50).Value = MaSV;
                cmd.Parameters.Add("HOTEN", SqlDbType.NVarChar, 50).Value = HoTen;
                cmd.Parameters.Add("NGAYSINH", SqlDbType.DateTime).Value = NgaySinh;
                cmd.Parameters.Add("DIACHI", SqlDbType.NVarChar, 100).Value = DiaChi;
                cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = MALOP;
                cmd.Parameters.Add("TENDN", SqlDbType.VarChar, 50).Value = TenDN;
                cmd.Parameters.Add("MATKHAU", SqlDbType.VarChar, 50).Value = MatKhau;
                cmd.ExecuteReader();
                frm_load();
            }

        }

        private void frmManagerStudent_MouseClick(object sender, MouseEventArgs e)
        {
            txtHoTen.ReadOnly = false;
            txtDiaChi.ReadOnly = false;
            txtTenDN.ReadOnly = false;
            txtMatKhau.ReadOnly = false;
            txtMaSV.ReadOnly = false;

            txtMaSV.Text = "";
            txtHoTen.Text = "";
            dateTimePicker1.Value = DateTime.Today;
            txtDiaChi.Text = "";
            txtTenDN.Text = "";
            txtMatKhau.Text = "";
        }

        private void btnDiem_Click(object sender, EventArgs e)
        {
            string Malop = MALOP;
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CHECK_NVCHAMDIEM", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = MALOP;
            SqlDataReader kq = cmd.ExecuteReader();
            kq.Read();
            if (kq[0].ToString() == MANV)
            {
                frmDiem frmDiem = new frmDiem(Malop, MANV);
                this.Hide();
                frmDiem.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("bạn không có quyền truy cập vào bảng đi");
            }
            
        }

        private int fetchSoLuongSinhVien(string Malop)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CNT_SV_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = Malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (kq.Read())
            {
                return kq.GetInt32(0);
            }
            else
            {
                return 0;
            }
        }
    }
}
