﻿
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace WinFormsApp
{
    public partial class Form1 : Form
    {
        string strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;       
        SqlCommand cmd;
        public Form1()
        {
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }
        private void btnlogin_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }

            var TaiKhoan = txtLogin.Text.Trim();
            string MatKhau = txtpassword.Text.Trim();
            byte[] MHMatKhau = CreateSHA(MatKhau);
            
            cmd = new SqlCommand("SP_CHECK_NHANVIEN_PASSWORD", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;
            cmd.Parameters.Add("@MATKHAU", SqlDbType.VarBinary).Value = MHMatKhau;
            SqlDataReader kq = cmd.ExecuteReader();
            
            if (kq.Read())
            {
                FrmManagerClass frmManagerClass = new FrmManagerClass(kq[1].ToString(),MatKhau);
                this.Hide();
                frmManagerClass.ShowDialog();
                this.Show();
            }
            else
            {
                cmd = new SqlCommand("SP_CHECK_SINHVIEN", sqlconn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;
                cmd.Parameters.Add("@MATKHAU", SqlDbType.VarBinary).Value = MHMatKhau;
                kq = cmd.ExecuteReader();
                if (kq.Read())
                {
                    MessageBox.Show("Chào bạn sinh viên" + kq[1].ToString());
                }
                else
                {
                    MessageBox.Show("tên đăng nhập và mật khẩu kh đúng");
                }
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public static byte[] CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                return hashBytes;

            }
        }
        public static byte[] CreateSHA(string input)
        {
            // Use input string to calculate MD5 hash
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = sha256.ComputeHash(inputBytes);

                return hashBytes;

            }
        }

        public static string ConvertByteToHexa(byte[] hashBytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}