﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WinFormsApp
{
    public partial class FrmManagerClass : Form
    {
        string strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        String MANV,MK;
        public FrmManagerClass(String Manv,String Mk)
        {
            this.MANV = Manv;
            this.MK = Mk;
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
        }

        private void FrmManagerClass_Load(object sender, EventArgs e)
        {
            frm_load();
        }
        private void frm_load()
        {
            cbxNhanVien.DisplayMember = "HOTEN";
            cbxNhanVien.DataSource = FetchNameNV();
            dGVLop.DataSource = FetchLop();
        }
        private DataTable FetchLop()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);

            return dt;
        }

        private DataTable FetchNameNV()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private void dGVLop_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {          
            if (e.RowIndex == -1) return;
            DataGridViewRow dataGridViewRow = dGVLop.Rows[e.RowIndex];
            txtMaLop.Text = dataGridViewRow.Cells[0].Value.ToString();
            txtTenLop.Text = dataGridViewRow.Cells[1].Value.ToString();
            txtMaLop.ReadOnly = true;
            cbxNhanVien.Text = dataGridViewRow.Cells[2].Value.ToString();           
        }
        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSua_Click(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CHECK_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (!kq.HasRows)
            {
                MessageBox.Show("Mã lớp không tồn tại!!");
            }
            else
            {
                try
                {
                    var tenlop = txtTenLop.Text.Trim();
                    var tennv = cbxNhanVien.Text.Trim();
                    if (sqlconn.State == ConnectionState.Closed)
                    {
                        sqlconn.Open();
                    }
                    cmd = new SqlCommand("SP_UPD_LOP", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
                    cmd.Parameters.Add("@TENLOP", SqlDbType.NVarChar, 100).Value = tenlop;
                    cmd.Parameters.Add("@HOTEN", SqlDbType.NVarChar, 50).Value = tennv;
                    cmd.ExecuteReader();
                    frm_load();
                }
                catch
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!!!");
                }
            }
        }


        

        private void frmManagerStudent_Click(object sender, EventArgs e)
        {
            txtMaLop.Text = "";
            txtMaLop.ReadOnly = false;
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CHECK_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (!kq.HasRows)
            {
                MessageBox.Show("Mã lớp không tồn tại!!");
            }
            else
            {
                try
                {
                    var tenlop = txtTenLop.Text.Trim();
                    var tennv = cbxNhanVien.Text.Trim();
                    if (sqlconn.State == ConnectionState.Closed)
                    {
                        sqlconn.Open();
                    }
                    cmd = new SqlCommand("SP_UPD_LOP", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
                    cmd.Parameters.Add("@TENLOP", SqlDbType.NVarChar, 100).Value = tenlop;
                    cmd.Parameters.Add("@HOTEN", SqlDbType.NVarChar, 50).Value = tennv;
                    cmd.ExecuteReader();
                    frm_load();
                }
                catch
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!!!");
                }
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }            
            cmd = new SqlCommand("SP_CHECK_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (kq.HasRows)
            {
                MessageBox.Show("Mã lớp đã tồn tại!!");
            }
            else
            {
                try
                {
                    var tenlop = txtTenLop.Text.Trim();
                    var tennv = cbxNhanVien.Text.Trim();
                    if (sqlconn.State == ConnectionState.Closed)
                    {
                        sqlconn.Open();
                    }
                    cmd = new SqlCommand("SP_INS_LOP", sqlconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@MALOP", SqlDbType.VarChar, 20).Value = malop;
                    cmd.Parameters.Add("@TENLOP", SqlDbType.NVarChar, 50).Value = tenlop;
                    cmd.Parameters.Add("@HOTEN", SqlDbType.NVarChar, 50).Value = tennv;
                    cmd.ExecuteNonQuery();
                    frm_load();
                }
                catch
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!!!");
                }

            }
        }

        private void btnHuy_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dGVLop_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            DataGridViewRow dataGridViewRow = dGVLop.Rows[e.RowIndex];
            string MALOP = dataGridViewRow.Cells[0].Value.ToString();
            frmManagerStudent frmManagerStudent = new frmManagerStudent(MANV, MALOP, MK);
            this.Hide();
            frmManagerStudent.ShowDialog();
            this.Show();
        }

        private void FrmManagerClass_Click(object sender, EventArgs e)
        {
            txtMaLop.ReadOnly = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var malop = txtMaLop.Text.Trim();
            string tenlop = txtTenLop.Text.ToString();
            if (malop == "")
            {
                MessageBox.Show("Hãy nhập mã lớp");
                return;
            }

            String message = "Bạn có chắc xoá lớp " + tenlop  + " ?";
            String title = "Thông Báo!";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons);
            if(result == DialogResult.Yes) {
                cmd = new SqlCommand("SP_DEL_LOP", sqlconn)
                {
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 20).Value = malop;
                cmd.ExecuteReader();
                frm_load();
            }
        }
    }
}
