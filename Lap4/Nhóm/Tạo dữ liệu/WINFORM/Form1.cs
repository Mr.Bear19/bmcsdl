﻿using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace WINFORM
{
    public partial class Form1 : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM_LAP4;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        public Form1()
        {
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }
        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
        byte[] plaintext;
        byte[] encryptedtext;
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }

            String Manv = txtManv.Text.Trim();
            String Hoten = txtHoten.Text.Trim();
            String Email = txtEmail.Text.Trim();
            String Luong = RSA.ExportParameters(false).ToString();
            String TenDN = txtTenDN.Text.Trim();
            String Matkhau = txtMK.Text.ToString();
            String PubKey; String PriKey;
            
            byte[] bytes = CreateSHA(Matkhau);
            //Matkhau = ConvertByteToHexa(bytes);
            //txtMH.Text = bytes;
            // mã hoá Lương
            plaintext = ByteConverter.GetBytes(txtLuong.Text);
            PubKey = RSA.ToXmlString(false);
            PriKey = RSA.ToXmlString(true);
            encryptedtext = RSAEncrypt(plaintext, RSA.ExportParameters(false),false);
            
            //Luong = ConvertByteToHexa(encryptedtext);

            cmd = new SqlCommand("SP_INS_PUBLIC_ENCRYPT_NHANVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MANV", SqlDbType.VarChar, 30).Value = Manv;
            cmd.Parameters.Add("HOTEN", SqlDbType.NVarChar, 50).Value = Hoten;
            cmd.Parameters.Add("EMAIL", SqlDbType.VarChar, 50).Value = Email;
            cmd.Parameters.Add("LUONG", SqlDbType.VarBinary).Value = encryptedtext;
            cmd.Parameters.Add("TENDN", SqlDbType.VarChar).Value = TenDN;
            cmd.Parameters.Add("MATKHAU", SqlDbType.VarBinary).Value = bytes;
            cmd.Parameters.Add("PUBKEY", SqlDbType.VarChar).Value = PubKey;
            cmd.Parameters.Add("PRIKEY", SqlDbType.VarChar).Value = PriKey;

            cmd.ExecuteReader();

        }
        //return byte[32]
        public static byte[] CreateSHA(string input)
        {
            // Use input string to calculate MD5 hash
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = sha256.ComputeHash(inputBytes);

                return hashBytes;

            }
        }
        //return byte[16]
        public static byte[] CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                return hashBytes;

            }
        }
        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            byte[] encryptedData;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);

                encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
            }
            return encryptedData;
        }
        public static string ConvertByteToHexa(byte[] hashBytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }

    }
}