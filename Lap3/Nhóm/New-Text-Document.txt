create table SINHVIEN(
	MASV varchar(20) primary key,
	HOTEN nvarchar(100) not null,
	NGAYSINH datetime null,
	DIACHI nvarchar(200) null,
	MALOP nvarchar(20) null,
	TENDN nvarchar(100) not null,
	MATKHAU varbinary(max) not null
)
go

create table NHANVIEN(
	MANV varchar(20) primary key,
	HOTEN nvarchar(100) not null,
	EMAIL VARCHAR (100) null,
	LUONG varbinary(max) null,
	TENDN nvarchar(100) not null,
	MATKHAU varbinary(max) not null,
	PUBKEY varchar(20)
)
go

create table LOP(
	MALOP varchar(20) primary key,
	TENLOP nvarchar(100),
	MANV varchar(20) null
)
go
create table HOCPHAN (
	MAHP varchar(20) primary key,
	TENHP nvarchar(100) not null,
	SOTC int
)
go
create table BANGDIEM(
	MASV varchar(20),
	MAHP varchar(20),
	DIEMTHI varbinary(max)
)
go

create master key

create procedure SP_INS_PUBLIC_NHANVIEN @MANV varchar(20), @HOTEN nvarchar(100), @EMAIL varchar (100),@LUONG int,@TENDN nvarchar(100),@MATKHAU varchar(1000)
as
begin
   DECLARE @C NVARCHAR(MAX) = 
    'create asymmetric key mahoaluong
        with algorithm = RSA_2048
        encryption by password = '''+@MATKHAU+''''
    EXEC(@C)

	insert into nhanvien values (@MANV,@HOTEN,@EMAIL,ENCRYPTBYASYMKEY(ASYMKEY_ID('mahoaluong'),CAST(@LUONG as varchar(100))),@TENDN,HASHBYTES('SHA1',@MATKHAU),@MANV)
end


EXEC SP_INS_PUBLIC_NHANVIEN 'NV03', 'NGUYEN VAN A', 'NVA@',
300000, 'NVA', 'abcd12'


select * from NHANVIEN where MANV ='NV03'
select * from LOP
-- cau ii
create procedure SP_SEL_PUBLIC_NHANVIEN @TENDN nvarchar(100),@MATKHAU varchar(1000)
as
begin
	select MANV, HOTEN, EMAIL,CONVERT(VARCHAR(max),  
    DecryptByAsymKey( AsymKey_Id('mahoaluong'),   
    LUONG, N''+@MATKHAU+'' ))  as LUONGCB
	from NHANVIEN
	where TENDN=@TENDN and MATKHAU=HASHBYTES('SHA1',@MATKHAU)
end 

exec  SP_SEL_PUBLIC_NHANVIEN 'NVA', 'abcd12'
--cau d
EXEC SP_INS_PUBLIC_NHANVIEN 'NV01', 'NGUYEN VAN A', 'nva@yahoo.com',
300000, 'NVA', '123456'
EXEC SP_INS_PUBLIC_NHANVIEN 'NV02', 'NGUYEN VAN B', 'nvb@yahoo.com',
200000, 'NVB', '1234567'
